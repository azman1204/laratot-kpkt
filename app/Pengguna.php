<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class Pengguna extends Authenticatable
{
    use HasRoles;
    protected $table = 'pengguna';
    protected $primaryKey = 'nokp';

    // replace email sbg default
    public function username()
    {
        return 'nokp';
    }

    // replace password sbg default
    public function getAuthPassword()
    {
        return $this->katalaluan;
    }

    public function pages()
    {
        return $this->hasMany('\App\Pages', 'author', 'nokp');
    }
}