<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Cookie;

class AuthController extends Controller
{
    public function form()
    {
        return view('login');
    }

    public function login(Request $req)
    {
        if (Auth::attempt(['nokp' => $req->nokp, 'password' => $req->katalaluan])) {
            // berjaya
            //return redirect('/home');
        } else {
            // gagal. with() = flash session data
            return redirect('/auth')->with('err', 'Gagal log masuk!');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/auth');
    }
}