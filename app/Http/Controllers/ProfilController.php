<?php
namespace App\Http\Controllers;

use App\Mail\LupaKatalaluan;
use App\Pengguna;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class ProfilController extends Controller
{
    // show form asking email
    public function lupaKatalaluan()
    {
        return view('profil.lupa_katalaluan');
    }

    public function hantarEmail(Request $req)
    {
        // jana token dan kemaskini table pengguna
        $token = md5(date('d/m/Y H:i:s'));
        echo $token;
        $p = Pengguna::where('emel', $req->emel)->first(); // return 1 rekod / 1 obj
        if ($p) {
            // pengguna wujud
            $p->remember_token = $token;
            $p->save();

            // send email
            Mail::to($req->emel)->send(new LupaKatalaluan($p));
        } else {
            // pengguna tak wujud
            return redirect('/profil/lupa-katalaluan')->with('err', 'Emel tidak wujud');
        }
    }

    //papar skrin utk kemaskini katalaluan
    public function kemaskiniKatalaluan($token)
    {
        $p = Pengguna::where('remember_token', $token)->first();
        if ($p) {
            // user wujud
            return view('profil.kemaskini_katalaluan', ['pengguna' => $p]);
        } else {
            echo 'No Permission';
        }
    }

    public function simpanKatalaluan(Request $req)
    {
        $p = Pengguna::find($req->nokp);
        $v = Validator::make($req->all(), ['katalaluan_baru' => 'required|confirmed']);

        if ($v->passes()) {
            $p->katalaluan = Hash::make($req->katalaluan_baru);
            $p->remember_token = null;
            $p->save();
            return redirect('/auth');
        } else {
            return redirect('/profil/kemaskini-katalaluan/' . $p->remember_token)
                ->with('err', 'Katalaluan dan pengesahan katalaluan tidak sama');
        }
    }
}