<?php

namespace App\Http\Controllers;

use App\Pengguna;

class PenggunaController extends Controller
{
    public function listing()
    {
        $pengguna = Pengguna::all();
        return view('pengguna.list', ['pengguna' => $pengguna]);
    }
}