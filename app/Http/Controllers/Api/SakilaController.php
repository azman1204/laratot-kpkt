<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class SakilaController extends \App\Http\Controllers\Controller {
    public function upload(Request $req) {
        //dd($req->all());
        $doc = $req->file('doc');
        //Storage::put('images', $doc, );
        try {
            $req->file('doc')->storeAs('images', $doc->getClientOriginalName());
            return ['status' => 'success'];
        } catch (\Exception $e) {
            return ['status' => 'fail'];
        }
    }

    public function delete($id) {
        $film = \App\Film::find($id);
        if ($film) {
            $film->delete();
            return ['status' => 'ok'];
        } else {
            // id x wujud
            return ['status' => 'ko'];
        }
    }

    public function update(Request $req) {
        //return $req->all();
        $film = \App\Film::find($req->id);
        if ($film) {
            $film->title = $req->title;
            $film->description = $req->description;
            $film->save();
            return $film;
        } else {
            return ['status' => '500', 'message' => 'ID tidak wujud'];
        }
    }

    public function insert(Request $req) {
        $film = new \App\Film();
        $film->title = $req->title;
        $film->description = $req->description;
        $film->release_year = $req->release_year;
        $film->save();
        return $film;
    }

    public function search(Request $req) {
        $title = $req->query('title');
        $descr = $req->query('descr');
        $query = \App\Film::where('title', 'LIKE', "%$title%");
        
        if (! empty($descr)) {
            $query = $query->orWhere('description', 'LIKE', "%$descr%");
        }
        
        $films = $query->paginate(20);
        return $films;
    }

    public function film($id) {
        $film = \App\Film::find($id);
        return $film;
    }

    // return first 20 rekod of film table
    public function filmList() {
        $films = DB::table('film')->paginate(20);
        return $films;
    }
}