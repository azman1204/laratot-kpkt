<?php
namespace App\Http\Controllers;

use App\Pages;
use Illuminate\Http\Request;
use Validator;

class PagesController extends Controller
{
    public function home()
    {
        // session('masa', date('d/m/Y H:i:s')) // salah
        session(['masa' => date('d/m/Y H:i:s')]); // set a value into a session
        $pages = Pages::all(); // return array of obj
        return view('home', ['pg' => $pages]); // resources/views/home.blade.php
    }

    public function add()
    {
        $pages = new Pages();
        return view('pages.add', ['pages' => $pages]); // resources/views/pages/add.blade.php
    }

    public function save(Request $req)
    {
        //return $req->all();
        //echo $req->title; // baca dari form
        $id = $req->id;

        if (empty($id)) {
            // insert
            $pages = new Pages();
        } else {
            // update
            $pages = Pages::find($id);
        }

        $pages->title = $req->title;
        $pages->content = $req->content;
        //validation
        $v = Validator::make($req->all(), $pages->rules(), $pages->messages());

        if (!$v->fails()) {
            // pass validation
            $pages->save();
            return redirect('/home');
        } else {
            // gagal validation
            return view('pages.add', ['pages' => $pages])->withErrors($v);
        }
    }

    public function edit($id)
    {
        //echo $id;
        // cari data yg hendak di update
        $pages = Pages::find($id);
        //dd($pages); // diedump
        // show form utk dikemaskini
        return view('pages.add', ['pages' => $pages]);
    }

    public function delete($id)
    {
        Pages::find($id)->delete();
        return redirect('/home');
    }
}