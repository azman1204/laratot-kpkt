<?php
namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class Security
{
    public function handle(Request $request, Closure $next)
    {
        //dd(Auth::user());
        //dd(Auth::check());
        if (Auth::check()) {
            // user telah logged-in
            return $next($request); // go to any route
        } else {
            // user belum login
            return redirect('/auth');
        }
    }
}