<?php
namespace App\Http\Middleware;
use Closure;
use App\Client;
use App\Log;
use Auth;
use Cookie;

class GuardApi
{
    public function handle($request, Closure $next)
    {
        $log = new Log();
        $log->ip = $request->ip();
        //$log->agent = $request->userAgent();
        $log->user_agent = $request->server('HTTP_USER_AGENT');
        $log->data = json_encode($request->all());
        $log->token = $request->header('Authorization');
        //$log->data_format = $request->header('Accept');
        $log->data_format = $request->headers->get('Content-Type');
        $log->method = $request->method();
        $log->url = $request->path();
        $log->save();

        dd(Cookie::get('loggedin'));

        // check user logged-in, the allow
        if (Auth::check()) {
            return $next($request);
        } else {
            //check token wujud dalam DB
            $client = Client::where('token', $request->token)->first();
            
            if (! $client) {
                // token tidak wujud
                return response()->json(['status' => 'token not valid']);
            }

            $ip = $request->ip();
            //echo $ip;exit;
            if ($client->ip !== $ip) {
                // ip tak berdaftar
                return response()->json(['status' => 'IP not valid']);
            }

            return $next($request);
        }
    }
}