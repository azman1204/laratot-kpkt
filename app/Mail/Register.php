<?php

namespace App\Mail;

use App\Pengguna;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Register extends Mailable
{
    use Queueable, SerializesModels;

    public $pengguna;

    public function __construct(Pengguna $pengguna)
    {
        $this->pengguna = $pengguna;
    }

    public function build()
    {
        return $this
            ->from('azman1204@yahoo.com')
            ->view('emails.senarai')
            ->with(['pengguna' => $this->pengguna]);
    }
}