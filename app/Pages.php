<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';

    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'content' => 'required|min:10',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Tajuk wajib diisi',
            'title.min' => 'Tajuk mestilah sekuran-kurangnya 3 karakter',
        ];
    }
}