<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('gen-token', function() {
    return Hash::make('1234');
});

Route::middleware('guardapi')->group(function() {
    // http://laratot.test/api/films
    Route::get('films', 'Api\SakilaController@filmList');

    // http://laratot.test/api/film/5
    Route::get('film/{id}', 'Api\SakilaController@film');

    // http://laratot.test/api/film-search?title=xxx&descr=yyy
    Route::get('film-search', 'Api\SakilaController@search');

    // http://laratot.test/api/film
    Route::post('film', 'Api\SakilaController@insert');

    // http://laratot.test/api/film
    Route::put('film', 'Api\SakilaController@update');

    // http://laratot.test/api/film
    Route::delete('film/{id}', 'Api\SakilaController@delete');

    // upload file
    Route::post('upload', 'Api\SakilaController@upload');
});


// http://laratot.test/api/hello
Route::get('hello', function() {
    //return 'Hello World';
    // jika return array / obj, laravel auto convert ke JSON
    return [
        'nama' => 'azman', 
        'alamat' => 'p.jaya'
    ];
});


