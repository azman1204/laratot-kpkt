<?php
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

Route::middleware('security')->group(function() {
    // http://laratot.test/api/films
    Route::get('films', 'Api\SakilaController@filmList');

    // http://laratot.test/api/film/5
    Route::get('film/{id}', 'Api\SakilaController@film');

    // http://laratot.test/api/film-search?title=xxx&descr=yyy
    Route::get('film-search', 'Api\SakilaController@search');

    // http://laratot.test/api/film
    Route::post('film', 'Api\SakilaController@insert');

    // http://laratot.test/api/film
    Route::put('film', 'Api\SakilaController@update');

    // http://laratot.test/api/film
    Route::delete('film/{id}', 'Api\SakilaController@delete');

    // upload file
    Route::post('upload', 'Api\SakilaController@upload');
});

// list all film using jQuery
Route::get('film-list', 'FilmController@list');

Route::get('films', 'Api\SakilaController@filmList');

Route::get('/modal', function () {
    return view('jquery.modal');
});

Route::get('/jq-sample1', function () {
    return view('jquery.sample1');
});

Route::get('/jq-masa', function () {
    return "Masa Server : " . date('d/m/Y H:i:s');
});

Route::get('/profil/lupa-katalaluan', 'ProfilController@lupaKatalaluan');
Route::post('/profil/lupa-katalaluan', 'ProfilController@hantarEmail');
Route::get('/profil/kemaskini-katalaluan/{token}', 'ProfilController@kemaskiniKatalaluan');
Route::post('/profil/kemaskini-katalaluan', 'ProfilController@simpanKatalaluan');

// test HTML Collective package
Route::get('/form', function () {
    return view('form');
});

Route::get('/auth', 'AuthController@form');
Route::post('/auth', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

Route::get('/mail', function () {
    $pengguna = \App\Pengguna::find('1111');
    Mail::to('azman1204@yahoo.com')
        ->send(new \App\Mail\Register($pengguna));
});

Route::get('/login', function () {
    if (Auth::attempt(['nokp' => '22225', 'password' => '1234'])) {
        echo 'berjaya';
    } else {
        echo 'gagal';
    }
});

// http://laratot.test
Route::get('/', function () {
    return view('welcome');
});

// http://laratot.test/hello
Route::get('/hello', function () {
    //return Hash::make('1234'); //generate password manually
    //return "Hello World";
});

Route::middleware(['security'])->group(function () {
    // http://laratot.test/home
    Route::get('/home', 'PagesController@home');
    Route::get('/pages/add', 'PagesController@add');
    Route::post('/pages/save', 'PagesController@save');
    Route::get('/pages/edit/{id}', 'PagesController@edit');
    Route::get('/pages/delete/{id}', 'PagesController@delete');

    Route::get('/pengguna', 'PenggunaController@listing');
});

// [your site path]/Http/routes.php
Route::any('captcha-test', function () {
    if (request()->getMethod() == 'POST') {
        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            echo '<p style="color: #ff0000;">Incorrect!</p>';
        } else {
            echo '<p style="color: #00ff30;">Matched :)</p>';
        }
    }

    $form = '<form method="post" action="captcha-test">';
    $form .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
    $form .= '<p>' . captcha_img() . '</p>';
    $form .= '<p><input type="text" name="captcha"></p>';
    $form .= '<p><button type="submit" name="check">Check</button></p>';
    $form .= '</form>';
    return $form;
});

Route::get('create-actor', function() {
    $actor = new \App\Actor();
    $actor->first_name = 'azman';
    $actor->last_name = 'zakaria';
    $addr = [
        ['street' => 'jalan 1', 'postcode' => '02200'],
        ['street' => 'jalan 2', 'postcode' => '05500']
    ];
    $actor->address = json_encode($addr);
    $actor->save();
});

Route::get('show-actor', function() {
    $actor = \App\Actor::find(201);
    $addr = json_decode($actor->address);
    foreach($addr as $a) {
        echo $a->street . '<br>';
    }
});