@extends('layout2')
@section('content')

@foreach ($errors->all() as $error)
<p class="alert alert-danger">{{ $error }}</p>
@endforeach

<form method="post" action="/pages/save">
    @csrf
    <input type="hidden" name="id" value="{{ $pages->id }}">
    Tajuk
    <br>
    <input type="text" class="form-control" name="title" value="{{ $pages->title }}">
    <br>
    Kandungan
    <br>
    <textarea class="form-control" name="content">{{ $pages->content }}</textarea>
    <br>
    <input type="submit" class="btn btn-primary" value="Simpan">
</form>
@endsection