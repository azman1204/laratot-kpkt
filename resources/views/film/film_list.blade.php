<script src="jquery.js"></script>

<h2>Film List</h2>

<table border="1" id="film">
    <tr>
        <td>Title</td>
        <td>Description</td>
    </tr>
</table>

<div id="pagination"></div>

<script>
$(function() {
    getData();

    

    function getData(page) {
        $.get('http://laratot.test/films?page='+page, function(data) {
            var arr = data.data;
            $('#film tr').remove();
            for(i=0; i<arr.length; i++) {
                row = arr[i];
                //console.log(row.title);
                tr = $('<tr><td>'+ row.title +'</td><td>' + row.description + '</td></tr>');
                $('#film').append(tr);
            }

            $('#pagination').html('');
            to = data.last_page;
            for(i=1; i<=to; i++) {
                li = "<li>" + i + "</li>";
                $('#pagination').append(li);
            }

            $('li').click(function() {
                page = $(this).text();
                getData(page);
            });
        });
    }
});
</script>

<style>
li {
    display: inline-block;
    border: 1px solid #ddd;
    padding: 2px;
    margin: 1px;
}
</style>