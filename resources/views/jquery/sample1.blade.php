@extends('layout')
@section('content')

<a href="#" class="btn btn-primary" id="butang">Click Me</a>
<a href="#" class="btn btn-primary" id="butang2">Show</a>

<span id="words" style="display:none;">
    Welcome to Narnia
    <span id="masa">...</span>
</span>

<script>
    $('#butang').click(function() {
    alert('Hello World from jQuery');
});

$('#butang2').click(function() {
    var txt = $('#butang2').text(); // return Show / Hide
    if (txt == 'Show') {
        $('#words').show('slow');
        $(this).text('Hide');
        $('#masa').load('http://laratot.test/jq-masa');
    } else {
        $('#words').hide('slow');
        $(this).text('Show');
    }
});
</script>

@endsection