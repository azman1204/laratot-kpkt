@extends('layout')
@section('content')

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#example">Open Modal</button>

<div class="modal" id="example">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Barang Sandaran</h5>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">Nama</div>
                    <div class="col-md-10"><input type="text" class="form-control"></div>
                </div>
                <span id="masa"></span>
            </div>

            <div class="modal-footer">
                <input type="button" value="Simpan" class="btn btn-primary">
            </div>
        </div>
    </div>
</div>

<script>
    $('#masa').load('{{ url('jq-masa') }}')
</script>

<style>
    .modal-header {
        background-color: #336699;
        color: white;
    }

    .modal-footer {
        background-color: #eee;
    }
</style>
@endsection