@extends('layout2')
@section('content')
<a href="/pages/add" class="btn btn-outline-success btn-xs">Tambah kandungan</a>
<br>
<?php
foreach ($pg as $p) {
    echo "{$p->title} <br>";
    echo "<a href='/pages/edit/{$p->id}' class='btn btn-warning btn-xs'>Edit</a> ";
    echo "<a href='/pages/delete/{$p->id}' class='btn btn-danger btn-xs' onclick='return confirm(\"Anda Pasti ? \")'>Hapus</a> <br>";
    echo "{$p->content} <hr>";
}
?>
@endsection